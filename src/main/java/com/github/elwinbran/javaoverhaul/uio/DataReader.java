
package com.github.elwinbran.javaoverhaul.uio;

import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;


/**
 * Reads an arbitrary data type from some data storage.
 * <br>The read location and reading is decided and handled by the subclasses.
 * 
 * @author Elwin Slokker
 * @param <DataT> The Output type. Preferably {@code String}, {@code List<String>} or {@code byte[]}.
 */
@FunctionalInterface
public interface DataReader<DataT>
{
    /**
     * Reads data from the source as type {@code D}.
     * 
     * @return A wrapper object that may carry the read data and/or an exception 
     * message, if anything went wrong. 
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<DataT> readSource();
}