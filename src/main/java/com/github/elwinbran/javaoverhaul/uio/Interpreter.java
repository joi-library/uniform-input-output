/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.uio;

import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;


/**
 * Interprets objects to and from some arbitrary data type.
 * Can also serve as some bi-directional factory.
 * 
 * @author Elwin Slokker
 * 
 * 
 */
public interface Interpreter<DataT>
{
    /**
     * Makes a {@code T} object from the data.
     * <br><br>
     * 
     * Architectural note: this could have had a normal return value and a
     * method that checked if {@code object} could be converter, but this 
     * requires the actual interpreting in most cases making it work twice.
     * 
     * @param data the data to be interpreted.
     * @return 
     */
    public abstract SafeReturn<CompositeSerializable> interpret(DataT data);
    
    /**
     * Makes data from the {@code T} object.
     * <br><br>
     * 
     * Architectural note: this could have had a normal return value and a
     * method that checked if {@code object} could be converter, but this 
     * requires the actual marshalling in most cases making it work twice.
     * 
     * @param object the data to be marshaled.
     * @return 
     */
    public abstract SafeReturn<DataT> marshal(CompositeSerializable object);
}
