
package com.github.elwinbran.javaoverhaul.uio;

import com.github.elwinbran.javaoverhaul.containers.Container;
import com.github.elwinbran.javaoverhaul.uei.ExceptionInformation;


/**
 * Writes an arbitrary data type to some data storage.
 * <br>The write location and writing is decided and handled by the subclasses.
 * 
 * @author Elwin Slokker
 * @param <DataT> The Input type. Preferably {@code List<String>} or {@code byte[]}.
 */
@FunctionalInterface
public interface DataWriter<DataT>
{
    /**
     * Writes the data to the source.
     * 
     * @param data Data to write.
     * @return
     */
    public abstract Container<ExceptionInformation> writeData(DataT data);
}