/**
 * So what if the user has to make a chain or map...
 * that handles different paths and protocols...
 * The handlers are serializable themself for maximum maintainability.
 * Obviously are there default local file and internet file handlers/providers
 * available otherwise it would be impossible to retrieve the settings.
 */
package com.github.elwinbran.javaoverhaul.uio;